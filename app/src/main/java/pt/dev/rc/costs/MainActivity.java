package pt.dev.rc.costs;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    public ArrayList<String> connections;
    ListView list;
    ArrayAdapter<String> lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JSONArray jsonArray;
        list = (ListView) findViewById(R.id.costs_list);

        FloatingActionButton add_btn = (FloatingActionButton) findViewById(R.id.add_btn);
        Bundle bundle;
        bundle = getIntent().getExtras();
        final String user_id = bundle.getString("user_id");

        add_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, FormActivity.class);
                    intent.putExtra("user_id", user_id);

                    MainActivity.this.startActivity(intent);
                }
            });

        try{

            jsonArray = new JSONArray(bundle.getString("items"));
            connections = new ArrayList<String>();
            //add all marker
            for(int i = 0; i < jsonArray.length();i++){
                JSONObject user = jsonArray.getJSONObject(i);
                String temp_name = user.getString("date")+"     "+user.getString("category")+"       € "+ user.getString("value");
                connections.add(temp_name);
            }

        } catch (JSONException e) {
            Toast.makeText(MainActivity.this,"Falha ao carregar os dados!",Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        lista = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1,connections);
        list.setAdapter(lista);

    }

}
