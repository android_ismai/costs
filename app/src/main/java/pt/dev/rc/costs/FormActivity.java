package pt.dev.rc.costs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormActivity extends AppCompatActivity {

    ImageView photo;
    Bitmap bitmap;
    ProgressDialog progress;
    private Spinner spinner_options;
    EditText form_description, form_value, form_date;


    private static final String saveUrl="http://a031017.000webhostapp.com/save.php";
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        photo = (ImageView) findViewById(R.id.img_form);

        Button btn_save = (Button) findViewById(R.id.btn_save);

        form_description  = (EditText) findViewById(R.id.description);
        form_value  = (EditText) findViewById(R.id.value);
        form_date  = (EditText) findViewById(R.id.date);

        FloatingActionButton camera = (FloatingActionButton) findViewById(R.id.btn_camera);

        addItemsOnSpinner();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,0);
            }
        });

        Bundle bundle = getIntent().getExtras();
        final String user_id = bundle.getString("user_id");

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress = new ProgressDialog(FormActivity.this);
                progress.setTitle("Carregando....");
                progress.setMessage("Aguarde enquanto os dados são guardados.");
                progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
                progress.show();

                // Preparação dos dados qe serão enviados no POST
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
                final String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(),Base64.DEFAULT);

                // POST Request
                StringRequest stringRequest = new StringRequest(Request.Method.POST, saveUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progress.dismiss();
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            if (success) {
                                //String user_id = jsonResponse.getString("user_id");

                                Toast.makeText(FormActivity.this,"Sucesso!",Toast.LENGTH_SHORT).show();

                                JSONArray items = jsonResponse.getJSONArray("items");

                                Intent intent = new Intent(FormActivity.this, MainActivity.class);
                                intent.putExtra("user_id", user_id);
                                intent.putExtra("items", items.toString());

                                FormActivity.this.startActivity(intent);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(FormActivity.this);
                                builder.setMessage("Falha ao salvar")
                                        .setNegativeButton("Tente novamente", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(FormActivity.this,"Erro!", Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    public Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> parameters = new HashMap<String,String>();
                        parameters.put("image", encodedImage);
                        parameters.put("user_id", user_id);
                        parameters.put("description",form_description.getText().toString());
                        parameters.put("value",form_value.getText().toString());
                        parameters.put("date",form_date.getText().toString());
                        parameters.put("category",spinner_options.getItemAtPosition(spinner_options.getSelectedItemPosition()).toString());

                        return parameters;
                    }
                };
                requestQueue = Volley.newRequestQueue(FormActivity.this);
                requestQueue.add((stringRequest));
            }
        });

    }

    // add items into spinner dynamically
    public void addItemsOnSpinner() {

        spinner_options = (Spinner) findViewById(R.id.spinner_opitions);
        List<String> list = new ArrayList<String>();
        list.add("Alimentação");
        list.add("Combustivel");
        list.add("Hospedagem");
        list.add("Portagens");
        list.add("Transporte");
        list.add("Outro");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_options.setAdapter(dataAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bitmap = (Bitmap)data.getExtras().get("data");
        photo.setImageBitmap(bitmap);
    }

}
