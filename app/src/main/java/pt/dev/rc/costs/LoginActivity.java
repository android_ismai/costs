package pt.dev.rc.costs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    // URL do servidor PHP - Pagina login
    private static final String loginUrl="http://a031017.000webhostapp.com/login.php";

    RequestQueue requestQueue;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText login_user = (EditText) findViewById(R.id.email);
        final EditText login_pwd = (EditText) findViewById(R.id.password);

        final Button btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = login_user.getText().toString();
                final String password = login_pwd.getText().toString();
                progress = new ProgressDialog(LoginActivity.this);
                progress.setTitle("Carregando....");
                progress.setMessage("Aguarde enquanto estabelece conexão com servidor.");
                progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
                progress.show();

                //insert user into server
                StringRequest stringRequest = new StringRequest(Request.Method.POST, loginUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progress.dismiss();
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");
                            if (success) {
                                String name = jsonResponse.getString("name");
                                String user_id = jsonResponse.getString("user_id");

                                Toast.makeText(LoginActivity.this,"Olá, "+name,Toast.LENGTH_SHORT).show();
                                JSONArray items = jsonResponse.getJSONArray("items");

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("user_id", user_id);
                                intent.putExtra("items", items.toString());

                                LoginActivity.this.startActivity(intent);

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage("Email ou Password incorretos")
                                        .setNegativeButton("Ok", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        Toast.makeText(LoginActivity.this,"Erro!", Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    public Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> parameters = new HashMap<String,String>();
                        parameters.put("username", username);
                        parameters.put("password", password);
                        return parameters;
                    }
                };
                requestQueue = Volley.newRequestQueue(LoginActivity.this);
                requestQueue.add((stringRequest));
            }
        });

    }
}
